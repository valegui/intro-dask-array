# Introducción a Dask Array

Presentación y notebook utilizados en la presentación de introducción a Dask impartida para DO-safíos Datatón FACH 2024.

Contenidos:
- Dask Collections
- Dask Array
- Funciones en bloque: `map_blocks` y `blockwise`
